<?php

use PHPUnit\Framework\TestCase;
use Dev\Db\DbQuery;

class DbQueryTest extends TestCase {

    protected $dbMock;
    protected $type = 1;

    public function setUp(): void {

        $statement = $this->getMockBuilder(PDOStatement::class)
                ->disableOriginalConstructor()
                ->getMock();

        $statement->method('execute')->willReturn(true);
        if ($this->type == 1) {
            $statement->method('fetch')->willReturn([
                'url' => 'https://2gis.ru/novosibirsk/geo/'
            ]);
        } else {
            $statement->method('fetch')->willReturn([]);
        }

        $this->dbMock = $this->getMockBuilder(PDO::class)
                ->disableOriginalConstructor()
                ->getMock();
        $this->dbMock->method('prepare')->willReturn($statement);
    }

    public function tearDown(): void {

        $this->dbMock = null;
        $this->type = null;
    }

    public function testGetQuery() {

        $sql = "select short from links where url = :url limit 1";
        $params = [':url' => $url];

        $DbQuery = new DbQuery($this->dbMock);
        $this->type = preg_match('/^select/i', $sql);
        $result = $DbQuery->getQuery($sql, $params);

        $this->assertNotEmpty($result);
        $this->assertEquals(['url' => 'https://2gis.ru/novosibirsk/geo/'], $result);

        $sql = "insert into links (url, created) values (:url, now())";
        $params = [':url' => $url];

        $this->type = preg_match('/^select/i', $sql);
        $result = $DbQuery->getQuery($sql, $params);

        $this->assertEmpty($result);
    }

}
