<?php

use PHPUnit\Framework\TestCase;
use Dev\Links\LinkController;
use Dev\Db\DbQuery;
use Dev\Links\Statistic;

class LinkControllerTest extends TestCase {

    protected $query;
    protected $stat;

    public function setUp(): void {

        $this->query = $this->createMock(DbQuery::class);
        $this->query->method('getQuery')
                ->willReturn([
                    'id' => '2',
                    'url' => 'https://2gis.ru/novosibirsk/geo/'
        ]);

        $this->stat = $this->createMock(Statistic::class);
        $this->stat->method('putVisits')
                ->willReturn('true');
    }

    public function tearDown(): void {

        $this->query = null;
        $this->stat = null;
    }

    public function testValidUrl() {

        $LinkContr = new LinkController;

        $urlRaw = 'https://2gis.ru/novosibirsk/geo/';
        $urlVal = $LinkContr->validUrl($urlRaw);
        $this->assertNotEmpty($urlVal);
        $this->assertEquals('https://2gis.ru/novosibirsk/geo/', $urlVal);

        $urlRaw = '';
        $urlVal = $LinkContr->validUrl($urlRaw);
        $this->assertEmpty($urlVal);

        $urlRaw = '2gis.ru';
        $urlVal = $LinkContr->validUrl($urlRaw);
        $this->assertEmpty($urlVal);
    }

    public function testGetUrl() {

        $sql = "select id, url from links where short = :short limit 1";
        $urlRaw = $this->query->getQuery($sql, [':short' => $short]);
        $id = $urlRaw['id'];
        $url = $urlRaw['url'];
        $this->stat->putVisits($id);

        $this->assertEquals('2', $id);
        $this->assertEquals('https://2gis.ru/novosibirsk/geo/', $url);
    }

}
