<?php

use PHPUnit\Framework\TestCase;
use Dev\Links\Shortener;

class ShortenerTest extends TestCase{
    
    public function testMakeShort() {

        $shortener = new Shortener;
        $id = '123';
        $short = $shortener->makeShort($id);
        
        $this->assertNotEmpty($short);
        $this->assertStringEndsNotWith('=', $short);
        $this->assertNotContains('=', $short);
    }
}