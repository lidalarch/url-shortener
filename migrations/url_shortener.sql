create database if not exists url_shortener;
use url_shortener;
CREATE USER if not exists 'url'@'localhost' IDENTIFIED BY '!V0Ht7v4quUY6@';
GRANT insert, select, update ON url_shortener. * TO 'url'@'localhost';
create table if not exists links (
    id int not null primary key auto_increment,
    url varchar(255) not null,
    short varchar(60),
    created datetime not null
) engine InnoDB;
create table if not exists visits (
    id int not null primary key auto_increment,
    id_link int not null,
    visited datetime not null
) engine InnoDB;
alter table visits add foreign key (id_link) references links (id);