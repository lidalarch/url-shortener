<?php
require_once __DIR__ . '/src/Dev/init.php';
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Сервис коротких ссылок</title>
        <link rel="stylesheet" href="style.css">
        <script defer src="js/jquery-3.4.1.min.js"></script>
        <script defer src="js/validate.js"></script>
        <script defer src="js/copy.js"></script>
    </head>
    <body>
        <div class="container">
            <h1 class="title">Сервис коротких ссылок</h1>
            <div>
                <form id="url_form" action="/commands/shorten.php" method="post">
                    <input id="url" type="url" name="url" placeholder="Введите URL" autocomplete="off" />
                    <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
                    <input id="subm" type="submit" value="Сократить" />
                    <div id="msg-box">
                    </div>
                </form>
            </div>
            <div>
                <form>
                    <input id="copyShort" type="url" name="url" placeholder="Короткая ссылка" autocomplete="off"
                           value="<?php
                           if (isset($_SESSION['short_msg'])) {
                               echo $_SESSION['short_msg'];
                               unset($_SESSION['short_msg']);
                           }
                           ?>" />
                    <input id="copyBtn" type="button" value="Скопировать" />
                </form>
            </div>
        </div> <!-- container-->
    </body>
</html>