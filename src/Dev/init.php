<?php

require_once __DIR__ . '/../../vendor/autoload.php';
session_start();

if (!isset($_SESSION['token'])) {
    $_SESSION['token'] = sha1(time());
}