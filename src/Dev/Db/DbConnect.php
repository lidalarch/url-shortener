<?php

namespace Dev\Db;

use PDO;
use Exception;
use Dev\Db\Config;

/**
 * Класс подключения к базе данных
 * @package Dev\Db
 */
class DbConnect {

    /**
     * @var PDO
     */
    public $db;

    /**
     * DbConnect constructor
     * @param PDO $db
     */
    public function __construct($dbo = null) {
        if (is_object($dbo)) {
            $this->db = $dbo;
        } else {
            try {
                $confPath = __DIR__ . '/../../config/config.json';
                $config = new Config($confPath);

                $dsn = "mysql:host={$config->serverName};port={$config->port};dbname={$config->dbase};charset=utf8";
                $username = $config->username;
                $password = $config->password;

                $connection = new PDO($dsn, $username, $password);
                $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db = $connection;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }

}
