<?php

namespace Dev\Db;

/**
 * Класс значений конфигурации
 * @package Dev\Db
 */
class Config {

    /**
     * @var string
     */
    public $serverName;
    public $port;
    public $dbase;
    public $username;
    public $password;

    /**
     * Config constructor
     * @param string $file
     * @throws Exception
     */
    public function __construct(string $file) {

        if (!file_exists($file)) {
            throw new Exception("Файл {$file} не найден");
        }
        if (!is_readable($file)) {
            throw new Exception("Файл {$file} недоступен для чтения");
        }
        $configArr = json_decode(file_get_contents($file), true);

        if ($configArr === false) {
            throw new Exception("Не удалось декодировать json");
        }
        foreach ($configArr as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else {
                throw new Exception("Свойство {$key} не найдено");
            }
        }
    }

}
