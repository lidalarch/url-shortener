<?php

namespace Dev\Db;

use Dev\Db\DbConnect;
use PDO;
use Exception;

/**
 * Класс для запросов к базе данных
 * @package Dev\Db
 */
class DbQuery extends DbConnect {

    /**
     * @param string $sql
     * @param array $params
     * @return array $result
     */
    public function getQuery(string $sql, array $params): array {

        $type = preg_match('/^select/i', $sql);
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->execute($params);
            if ($type == 1) {
                $result = (array) $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                $result = [];
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }

        return $result;
    }

}
