<?php

namespace Dev\Links;

use Dev\Db\DbQuery;
use Dev\Links\Shortener;

/**
 * Класс для управления переданными ссылками
 * @package Dev\Links
 */
class LinkController {

    /**
     * @param string $url
     * @return string $short
     */
    public function getShort($url): string {

        $DbQuery = new DbQuery;
        $sql = "select short from links where url = :url limit 1";
        $shortRaw = $DbQuery->getQuery($sql, [':url' => $url]);

        if (isset($shortRaw) && (!is_null($shortRaw['short']))) {
            $short = $shortRaw['short'];

            return $short;
        } else {
            $sql = "insert into links (url, created) values (:url, now())";
            $DbQuery->getQuery($sql, [':url' => $url]);

            $sql = "select id from links where url = :url limit 1";
            $idRaw = $DbQuery->getQuery($sql, [':url' => $url]);
            $id = $idRaw['id'];

            $shortener = new Shortener();
            $short = $shortener->makeShort($id);

            $sql = "update links set short = :short where id = :id";
            $DbQuery->getQuery($sql, [':short' => $short, ':id' => $id]);

            return $short;
        }
    }

    /**
     * @param string $short
     * @return string $url
     */
    public function getUrl($short): string {

        $DbQuery = new DbQuery;
        $sql = "select id, url from links where short = :short limit 1";
        $urlRaw = $DbQuery->getQuery($sql, [':short' => $short]);
        $id = $urlRaw['id'];
        $url = $urlRaw['url'];

        $stat = new Statistic();
        $stat->putVisits($id);

        return $url;
    }

    /**
     * @param string $urlRaw
     * @return string $urlVal
     */
    public function validUrl($urlRaw): string {
        $url = htmlspecialchars($urlRaw);
        $urlVal = filter_var($url, FILTER_VALIDATE_URL);

        return $urlVal;
    }

}
