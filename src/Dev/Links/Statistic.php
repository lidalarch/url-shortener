<?php

namespace Dev\Links;

use Dev\Db\DbQuery;

/**
 * Класс для заполнения БД статистики
 * @package Dev\Links
 */
class Statistic {

    /**
     * @param string $id
     * @return bool
     */
    public function putVisits($id) {

        $DbQuery = new DbQuery;
        $sql = "insert into visits (id_link, visited) values (:id, now())";
        $DbQuery->getQuery($sql, [':id' => $id]);
    }

}
