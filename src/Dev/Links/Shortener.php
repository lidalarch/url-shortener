<?php

namespace Dev\Links;

/**
 * Класс для алгоритма сокращения ссылок
 * @package Dev\Links
 */
class Shortener {

    /**
     * @param string $id
     * @return string $short
     */
    public function makeShort($id) {

        $shortRaw = base64_encode($id);
        $short = preg_replace('/=+$/', '', $shortRaw);

        return $short;
    }

}
