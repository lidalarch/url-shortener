$(document).ready(function () {

    $('input#url').unbind().blur(function () {  //on losing focus
        
        var val = $(this).val();       
        var regex_url = /^https?:\/\/([\w]+\.)*[\w]+\.[a-z]{2,6}($|\/.*)/;
        
        if (val == '') {
            $(this).removeClass().addClass('empty');
            $('div#msg-box').html('');
            
        } else if (regex_url.test(val)) {
            $(this).removeClass().addClass('right');
            $('div#msg-box').html('Принято')
                    .css('color', 'green')
                    .animate({'paddingLeft': '10px'}, 400)
                    .animate({'paddingLeft': '5px'}, 400);
        } else {
            $(this).removeClass().addClass('wrong');
            $('div#msg-box').html('поле должно содержать корректный url-адрес')
                    .css('color', 'red')
                    .animate({'paddingLeft': '10px'}, 400)
                    .animate({'paddingLeft': '5px'}, 400);
        }

    });

    $('input#subm').click(function (e) {

        e.preventDefault();
        
        if ($('.empty').length == 1) {
            $('div#msg-box').html('поле url обязательно для заполнения')
                    .css('color', 'red')
                    .animate({'paddingLeft': '10px'}, 400)
                    .animate({'paddingLeft': '5px'}, 400);
            
        } else if ($('.right').length == 1) {
            $('form#url_form').submit();
            $('input#url').val('').removeClass();
            $('div#msg-box').html('');
            
        } else {
            return false;
        }
    });
});
