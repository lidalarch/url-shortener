$(document).ready(function () {

    var copyShort = document.getElementById('copyShort');
    var copyBtn = document.getElementById('copyBtn');

    var copyToClipboard = function(){
        copyShort.select();
        document.execCommand('copy');
        copyShort.blur();  
    };

    copyBtn.addEventListener('click',copyToClipboard);

    window.onunload = function(){
        copyBtn.removeEventListener('click',copyToClipboard);
    };
});