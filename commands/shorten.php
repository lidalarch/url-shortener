<?php

require_once __DIR__ . '/../src/Dev/init.php';

use Dev\Links\LinkController;

$linkContr = new LinkController();

if (isset($_POST['url']) && ($_POST['token'] == $_SESSION['token'])) {
    $url = $linkContr->validUrl($_POST['url']);
    
    if ($url == false) {
        $_SESSION['short_msg'] = "Ошибка. Возможно, некорректный URL";
        
    } else {
        $link = $linkContr->getShort($url);
        if (!is_null($link)) {
            $_SESSION['short_msg'] = "http://url-shortener/{$link}";
        } else {
            $_SESSION['short_msg'] = "Ошибка";
        }
    }
}
header('Location: ../index.php');
