<?php

require_once __DIR__ . '/../src/Dev/init.php';

use Dev\Links\LinkController;

if (isset($_GET['short'])) {
    $short = htmlspecialchars($_GET['short']);
    $linkContr = new LinkController();
    $url = $linkContr->getUrl($short);
    if (!is_null($url)) {
        header("Location: {$url}");
        exit();
    }
}

header('Location: ../index.php');
